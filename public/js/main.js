const bankBalanceElement = document.getElementById("bankBalance");
const bankLoanNameElement = document.getElementById("bankLoanName");
const outstandingLoanElement = document.getElementById("outstandingLoan");
const workPayElement = document.getElementById("workPay");
const addLoanBtnElement = document.getElementById("addLoanBtn");
const addBalanceBtnElement = document.getElementById("addBalanceBtn");
const payLoanBtnElement = document.getElementById("payLoanBtn");
const workBtnElement = document.getElementById("workBtn");
const buyNowBtnElement = document.getElementById("buyNowBtn");
const laptopElement = document.getElementById("laptop");
const featuresElement = document.getElementById("features");
const laptopNameElement = document.getElementById("laptopName");
const laptopDescriptionElement = document.getElementById("laptopDescription");
const laptopPriceElement = document.getElementById("laptopPrice");
const imagePriceElement = document.getElementById("image");

let bankLoan = 0;
let hasLoan = false;
let bankBalance = 0;
let tmpPay = 0;
let pay = 0;
let price = 0;
let laptops = [];
const URL = "https://noroff-komputer-store-api.herokuapp.com/";
// get computer API
fetch(URL + "computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToMenu(laptops));

// adds the laptop data to the menu and the laptop display
const addLaptopsToMenu = (laptops) => {
  laptops.forEach((x) => addLaptopToMenu(x));
  // setts index 0 to load when page loads
  laptopNameElement.innerText = laptops[0].title;
  featuresElement.innerText = laptops[0].specs;
  laptopDescriptionElement.innerText = laptops[0].description;
  laptopPriceElement.innerText = laptops[0].price + " NOK";
  price = laptops[0].price;
  imagePriceElement.src = URL + laptops[0].image;
  
};
// adds the laptop names top the dropdown menu
const addLaptopToMenu = (laptop) => {
  const newLaptopElement = document.createElement("option");
  newLaptopElement.value = laptop.id;
  newLaptopElement.appendChild(document.createTextNode(laptop.title));
  laptopElement.appendChild(newLaptopElement);
};
// handle the menu changes for the laptops
const handleLaptopMenuChange = (e) => {
  const selectedLaptop = laptops[e.target.selectedIndex];
  laptopNameElement.innerText = selectedLaptop.title;
  featuresElement.innerText = selectedLaptop.specs;
  laptopDescriptionElement.innerText = selectedLaptop.description;
  laptopPriceElement.innerText = selectedLaptop.price + " NOK";
  price = selectedLaptop.price;
  imagePriceElement.src = URL + selectedLaptop.image;
};

laptopElement.addEventListener("change", handleLaptopMenuChange);
// adds loan to balance
addLoanBtnElement.addEventListener("click", function (event) {
  event.preventDefault();

  if (!hasLoan) {
    let result = window.prompt("How much loan?");
    result = parseInt(result);
    if (result <= bankBalance * 2) {
      bankLoanNameElement.innerText = "Loan: ";
      bankLoan += result;
      bankBalance += bankLoan;
      outstandingLoanElement.innerText = bankLoan + "Kr.";
      bankBalanceElement.innerText = bankBalance + " Kr.";
      hasLoan = true;
    } else {
      alert("You can not loan more then " + bankBalance * 2 + ".");
    }
  } else {
    alert("You already have a loan!");
  }
  checkLoan(hasLoan);
});
// add money for the pay account to the balance account
addBalanceBtnElement.addEventListener("click", function (event) {
  event.preventDefault();
  let tenPrecent = 0;
  if (hasLoan) {
    tenPrecent = pay * 0.1;
    if (tenPrecent > bankLoan) {
      tenPrecent = bankLoan;
    }
    bankLoan -= tenPrecent;
    outstandingLoanElement.innerText = bankLoan + " Kr.";
    pay -= tenPrecent;
  }

  bankBalance += pay;
  pay = 0;
  workPayElement.innerText = pay + " Kr.";
  bankBalanceElement.innerText = bankBalance + " Kr.";
  if (bankLoan <= 0) {
    bankLoan = 0;
    hasLoan = false;
  }
  checkLoan(hasLoan);
});
// pays the loan of for the pay account
payLoanBtnElement.addEventListener("click", function (event) {
  event.preventDefault();
  if (pay < bankLoan) {
  }
  tmpPay = pay;
  tmpPay = tmpPay - bankLoan;
  bankLoan -= pay;
  if (bankLoan <= 0) {
    bankLoan = 0;
    hasLoan = false;
  }
  pay = tmpPay;
  if (pay <= 0) {
    pay = 0;
  }
  workPayElement.innerText = pay + " Kr.";
  outstandingLoanElement.innerText = bankLoan + " Kr.";
  checkLoan(hasLoan);
});
// add 100 kr to the pay account when pressed
workBtnElement.addEventListener("click", function (event) {
  event.preventDefault();
  pay += 100;
  workPayElement.innerText = pay + " Kr.";
});
// buys the selected laptop with balance account
buyNowBtnElement.addEventListener("click", function (event) {
  event.preventDefault();

  if (bankBalance >= price) {
    bankBalance -= price;
    bankBalanceElement.innerText = bankBalance + " Kr.";
    alert("Congratulation! You now own a new PC!");
  } else {
    alert("You don't have enough money to buy this PC!");
  }
});
// toggles the pay loan button
function checkLoan(checkingLoan) {
  if (checkingLoan) {
    payLoanBtnElement.style.visibility = "visible";
  } else {
    payLoanBtnElement.style.visibility = "hidden";
    bankLoanNameElement.innerText = "";
    outstandingLoanElement.innerText = "";
  }
}

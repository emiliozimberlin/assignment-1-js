# The Komputer Store
A dynamic webpage using “vanilla” JavaScript simulating a computer store. 
## Table of Contents
- [Description](#Description)
- [Installation](#Installation)
- [Components](#Components)
- [Maintainer](#Maintainer)
- [Author](#Author)
- [License](#License)
## Description
This is a dynamic webpage using “vanilla” JavaScript simulating a computer store. 
Where you can work to earn money and/or take bank loans.
You can pay your loans and/or buy laptops.
The data for the laptops will be provided to you via a RESTful API.
## Installation
Run it in Visual Studios code, then install the extension Live Server. Right click the html file, then select open with Live Server.

## Components
This project contains A html file, javaScript file and a css file.

## Maintainer
[@emiliozimberlin](https://gitlab.com/emiliozimberlin)

## Author
Emilio Zimberlin
## License
[MIT](https://choosealicense.com/licenses/mit/)
